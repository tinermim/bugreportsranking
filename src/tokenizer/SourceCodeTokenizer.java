package tokenizer;

import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.standard.ClassicAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.tartarus.snowball.ext.PorterStemmer;

import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;


class LuceneConstants {
    public static final String CONTENTS="contents";
    public static final String FILE_NAME="filename";
    public static final String FILE_PATH="filepath";
    public static final int MAX_SEARCH = 10;
}

/**
 * Created by ramtin on 15/7/8 AD.
 *  This class provides static methods which are able to tokenize a document, remove stop words and other unimportant tokens
 *  and also stem an array of tokens
 */
public final class SourceCodeTokenizer {
    private SourceCodeTokenizer(){};

    /**
     * Uses Apache NLP library to tokenize the given string
     *
     * @param text
     * @return tokens
     * @throws FileNotFoundException
     */
    public static String[] openNlpTokenizer(String text){
        InputStream modelIn = null;
        try {
            modelIn = new FileInputStream("assets/en-token.bin");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String[] tokens = null;
        try {
            assert modelIn != null;
            AtomicReference<TokenizerModel> model = new AtomicReference<TokenizerModel>(new TokenizerModel(modelIn));
            Tokenizer tokenizer = new TokenizerME(model.get());
            tokens = tokenizer.tokenize(text);
          //  System.out.println("openNLP tokenizer: ");
//            for(int i = 0 ; i < tokens.length; i++)
//                System.out.print("[" + tokens[i] + "] ");
//            System.out.println("\n");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (modelIn != null) {
                try {
                    modelIn.close();
                } catch (IOException e) {
                }
            }
            return tokens;
        }
    }

    /**
     * Uses lucene analyzers to tokenize, remove stopWords and stem the text(based on the analyzer).
     * @param text text to be analyze
     * @param c Lucene analyzer class that is going to be used as the analyzer
     * @return analyzed tokens
     */
    public static String AnalyzeUsingLucene(String text, Class c){
        Analyzer analyzer = null;
        try {
            analyzer = (Analyzer) c.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        ArrayList<String> tokens = new ArrayList<String>();

        TokenStream tokenStream = null;
        try {
            assert analyzer != null;
            tokenStream = analyzer.tokenStream(
                    LuceneConstants.CONTENTS, new StringReader(text));
        } catch (IOException e) {
            e.printStackTrace();
        }
        CharTermAttribute term = tokenStream.addAttribute(CharTermAttribute.class);
        try {
            tokenStream.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String result = "";
        try {
           // System.out.println("lucene " + c.getSimpleName()+":");
            while(tokenStream.incrementToken()) {
                if(term.length() > 1 && !term.toString().matches("\\d+") &&
                        !term.toString().matches("\\d+\\.\\d+")) {
                    //System.out.print("[" + term.toString() + "] ");
                    //tokens.add(term.toString());
                    result += term.toString() + " ";
                }
            }
          //  System.out.println("\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private static void addCompoundWords() {
        //TODO
    }

    /**
     * stem an array of tokens using Porter Stemmer algorithm
     *
     * @param tokens
     * @return
     */
    public static String[] stem(String[] tokens) {
        for(int i = 0 ; i < tokens.length; i++){
            PorterStemmer stemmer = new PorterStemmer();
            stemmer.setCurrent(tokens[i]); //set string you need to stem
            stemmer.stem();  //stem the word
            tokens[i] = stemmer.getCurrent();
        }
        return tokens;
    }


    /**
     * Test
     * @param args
     */
    public static void main(String[] args){
        String documentContent = "at 8 o'clock we're workBench workbench work-bench work-Bench For a source file, we use its whole content – code and comments. To tokenize an input document, we first split\n" +
                " the text into a bag of words using white spaces. We then remove punctuation, numbers, and standard IR stop\n" +
                " words such as conjunctions or determiners. Compound words such as “WorkBench” are split into their components\n" +
                " based on capital letters, although more sophisticated methods could have been used here too.\n" +
                " The bag of words representation of the document is then augmented with the resulting tokens – “Work” and “Bench”\n" +
                " in this example – while also keeping the original word as a token. Finally, all words are reduced to their\n" +
                " stem using the Porter stemmer";

        openNlpTokenizer(documentContent);
        /* Tokenize */
        AnalyzeUsingLucene(documentContent, SimpleAnalyzer.class);

        /* Tokenize + StopWords */
        AnalyzeUsingLucene(documentContent, StandardAnalyzer.class);
        AnalyzeUsingLucene(documentContent, ClassicAnalyzer.class);

        /* Tokenize + StopWords + Stemming*/
        AnalyzeUsingLucene(documentContent, EnglishAnalyzer.class);

        //addCompoundWords();
        //stem();

        //TODO after knowing which of these analyzers and tokenizer should be used, return the proper object
    }
}
