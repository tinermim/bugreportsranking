/**
 * Created by ramtin on 15/7/8 AD.
 * For a source file, we use its whole content – code and comments. To tokenize an input document, we first split
 * the text into a bag of words using white spaces. We then remove punctuation, numbers, and standard IR stop
 * words such as conjunctions or determiners. Compound words such as “WorkBench” are split into their components
 * based on capital letters, although more sophisticated methods could have been used here too.
 * The bag of words representation of the document is then augmented with the resulting tokens – “Work” and “Bench”
 * in this example – while also keeping the original word as a token. Finally, all words are reduced to their
 * stem using the Porter stemmer
 */
package tokenizer;