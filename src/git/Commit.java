package git;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Commit {
	private String ID;
	private int year;
	private int month;
	private int day;
	private int hour;
	private String author;
	private ArrayList<String> files;
	private int index;
	private String date;

	public Commit(String ID, int dateSec, String author, int index) {
		this.ID = ID;
		this.author = author;
		this.index = index;
		setDate(((long) (dateSec)) * 1000);
	}

	private void setDate(long dateMiliSec) {
		Date date = new Date(dateMiliSec);
		SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		this.date = df2.format(date);
		String[] dateTemp = this.date.split("-");
		year = Integer.parseInt(dateTemp[0]);
		month = Integer.parseInt(dateTemp[1]);
		day = Integer.parseInt(dateTemp[2]);
		hour = Integer.parseInt(dateTemp[3]);
	}

	public void setFiles(ArrayList<String> files) {
		this.files = files;
	}

	public boolean containFile(String fileName) {
		for (String string : files)
			if (string.contains(fileName))
				return true;
		return false;
	}

	public boolean isInRange(String givenTimeSec) {
		long givenTimeMiliSec = Long.parseLong(givenTimeSec);
		Date date = new Date(givenTimeMiliSec);
		SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		String givenDateTemp = df2.format(date);
		String[] givenDate = givenDateTemp.split("-");
		int givenYear = Integer.parseInt(givenDate[0]);
		int givenMonth = Integer.parseInt(givenDate[1]);
		int givenDay = Integer.parseInt(givenDate[2]);

		if (givenYear - year <= 1) {
			if (givenMonth == month && givenYear == year)
				return true;
			else if (givenMonth - month == 1 && givenYear == year
					&& givenDay < day)
				return true;
			else if (givenYear != year && month - givenMonth == 11
					&& day >= givenDay)
				return true;
		}
		return false;
	}

	public String getID() {
		return this.ID;
	}

	public String getAuthor() {
		return this.author;
	}

	public int getIndex() {
		return this.index;
	}

	public String getMapKey() {
		String normalMonth, normalDay;
		if (month < 10)
			normalMonth = "0" + month;
		else
			normalMonth = month + "";
		if (day < 10)
			normalDay = "0" + day;
		else
			normalDay = day + "";
		return year + "-" + normalMonth + "-" + normalDay;
	}

	public String getCommitDate() {
		return this.date;
	}
}
