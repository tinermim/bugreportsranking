package git;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.CheckoutConflictException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRefNameException;
import org.eclipse.jgit.api.errors.RefAlreadyExistsException;
import org.eclipse.jgit.api.errors.RefNotFoundException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.diff.RawTextComparator;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.util.io.DisabledOutputStream;

import utils.Debug;

public class CrawlHandler {
	private ArrayList<Commit> repoCommits;
	private ArrayList<RevCommit> repoWalks;
	private Git git;
	private RevWalk walk;
	private Repository repository;
	private DiffFormatter formatter;
	private HashMap<String, Integer> commitMap;

	public CrawlHandler(String repoAddr) throws IOException {
		initializeRepository(repoAddr);
	}

	private void initializeRepository(String repoAddr) {
		repoWalks = new ArrayList<RevCommit>();
		repoCommits = new ArrayList<Commit>();
		commitMap = new HashMap<String, Integer>();
		FileRepositoryBuilder builder = new FileRepositoryBuilder();
		try {
			repository = builder.setGitDir(new File(repoAddr))
					.readEnvironment().findGitDir().build();
		} catch (Exception e) {
			System.out.println("git address is not valid");
		}
		git = new Git(repository);
		walk = new RevWalk(repository);
		getAllCommits();
	}

	private void getAllCommits() {
		try {
			List<Ref> branches = git.branchList().call();

			for (Ref branch : branches) {
				String branchName = branch.getName();

				Iterable<RevCommit> commits = git.log().all().call();

				for (RevCommit commit : commits) {
					boolean foundInThisBranch = false;

					RevCommit targetCommit = walk.parseCommit(repository
							.resolve(commit.getName()));
					for (Map.Entry<String, Ref> e : repository.getAllRefs()
							.entrySet())
						if (e.getKey().startsWith(Constants.R_HEADS))
							if (walk.isMergedInto(targetCommit, walk
									.parseCommit(e.getValue().getObjectId()))) {
								String foundInBranch = e.getValue().getName();
								if (branchName.equals(foundInBranch)) {
									foundInThisBranch = true;
									break;
								}
							}

					if (foundInThisBranch) {
						repoWalks.add(commit);
						Commit newCommit = new Commit(
								commit.getId().toString(),
								commit.getCommitTime(), commit.getAuthorIdent()
										.getName(), repoCommits.size());
						newCommit.setFiles(getFilesInCommit(commit.getName(),
								repoWalks.size() - 1));
						repoCommits.add(newCommit);
						commitMap.put(newCommit.getMapKey(),
								newCommit.getIndex());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void getDiff(String oldCommitId, String newCommitId) {
		try {
			AbstractTreeIterator oldTreeParser = prepareTreeParser(repository,
					oldCommitId);
			AbstractTreeIterator newTreeParser = prepareTreeParser(repository,
					newCommitId);

			List<DiffEntry> diff = git.diff().setOldTree(oldTreeParser)
					.setNewTree(newTreeParser).call();
			for (DiffEntry entry : diff) {
				System.out.println("Entry: " + entry + ", from: "
						+ entry.getOldId() + ", to: " + entry.getNewId());
				formatter = new DiffFormatter(System.out);
				formatter.setRepository(repository);
				formatter.format(entry);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Set<String> getModifierAuthors(String fileName,
			String givenCommitReportDate) {
		Set<String> authors = new HashSet<String>();
		String[] temp = fileName.split("/");
		String fileNameSplitted = temp[temp.length - 3] + "/"
				+ temp[temp.length - 2] + "/" + temp[temp.length - 1];
		Debug.log("the file name checking : " + fileNameSplitted);
		String key = getProperKey(givenCommitReportDate);
		if (key.equals("invalid"))
			return authors;
		int givenCommitIndex = commitMap.get(key);
		for (int i = givenCommitIndex; i < repoCommits.size(); i++) {
//			if (!repoCommits.get(i).isInRange(givenCommitReportDate))
//				break;
			if (repoCommits.get(i).containFile(fileNameSplitted)) {
				authors.add(repoCommits.get(i).getAuthor());
				Debug.log("found the author '" + repoCommits.get(i).getAuthor()
						+ "' who modified the file");
			}
		}
		return authors;
	}

	// private boolean validTimeReport(String givenCommitReportDate) {
	// String[] givenDate = getProperKey(givenCommitReportDate).split("-");
	// String[] minDate = repoCommits.get(repoCommits.size() - 1)
	// .getCommitDate().split("-");
	// String[] maxDdate = repoCommits.get(0).getCommitDate().split("-");
	// int[] givenDateNum = new int[3];
	// int[] maxDateNum = new int[3];
	// int[] minDateNum = new int[3];
	//
	// for (int i = 0; i < 3; i++) {
	// givenDateNum[i] = Integer.parseInt(givenDate[i]);
	// maxDateNum[i] = Integer.parseInt(maxDdate[i]);
	// minDateNum[i] = Integer.parseInt(minDate[i]);
	// }
	//
	// if ((givenDateNum[0] < maxDateNum[0] && givenDateNum[0] > minDateNum[0])
	// || (givenDateNum[0] <= maxDateNum[0] && givenDateNum[0] >= minDateNum[0])
	// && (givenDateNum[1] < maxDateNum[1] && givenDateNum[1] > minDateNum[1])
	// || (givenDateNum[0] <= maxDateNum[0] && givenDateNum[0] >= minDateNum[0])
	// && (givenDateNum[1] <= maxDateNum[1] && givenDateNum[1] >= minDateNum[1])
	// && (givenDateNum[2] <= maxDateNum[2] && givenDateNum[2] >=
	// minDateNum[2])) {
	// return true;
	// }
	// return false;
	// }

	private String getProperKey(String givenCommitReportDate) {
		Date date = new Date(Long.parseLong(givenCommitReportDate));
		SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		String givenDateTemp = df2.format(date);
		String[] givenDate = givenDateTemp.split("-");
		int givenYear = Integer.parseInt(givenDate[0]);
		int givenMonth = Integer.parseInt(givenDate[1]);
		int givenDay = Integer.parseInt(givenDate[2]);
		return checkKey(givenYear, givenMonth, givenDay);
	}

	private String checkKey(int year, int month, int day) {
		int counter = 0;
		while (commitMap.get(getNormalizedKeyDate(year, month, day)) == null) {
			counter++;
			if (counter == 31)
				return "invalid";
			if (day != 1)
				day--;
			else {
				day = 31;
				if (month != 1)
					month--;
				else {
					month = 12;
					year--;
				}
			}
		}
		return getNormalizedKeyDate(year, month, day);
	}

	private String getNormalizedKeyDate(int year, int month, int day) {
		String normalMonth, normalDay;
		if (month < 10)
			normalMonth = "0" + month;
		else
			normalMonth = month + "";
		if (day < 10)
			normalDay = "0" + day;
		else
			normalDay = day + "";
		return year + "-" + normalMonth + "-" + normalDay;
	}

	public void checkOut(String commitID) throws RefAlreadyExistsException,
			RefNotFoundException, InvalidRefNameException,
			CheckoutConflictException, GitAPIException {
		git.checkout().setName(commitID).call();
	}

	private AbstractTreeIterator prepareTreeParser(Repository repository,
			String objectId) {
		CanonicalTreeParser oldTreeParser = null;
		try {
			RevCommit commit = walk.parseCommit(ObjectId.fromString(objectId));
			RevTree tree = walk.parseTree(commit.getTree().getId());

			oldTreeParser = new CanonicalTreeParser();
			try (ObjectReader oldReader = repository.newObjectReader()) {
				oldTreeParser.reset(oldReader, tree.getId());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		walk.dispose();

		return oldTreeParser;
	}

	private ArrayList<String> getFilesInCommit(String commitId, int index) {
		RevCommit commit = repoWalks.get(index);
		ArrayList<String> list = new ArrayList<String>();
		if (!hasCommits(repository)) {
			return list;
		}
		try {
			if (commit.getParentCount() == 0) {
				TreeWalk tw = new TreeWalk(repository);
				tw.reset();
				tw.setRecursive(true);
				tw.addTree(commit.getTree());
				while (tw.next()) {
					list.add("added " + tw.getPathString());
				}
				tw.close();
			} else {
				RevCommit parent = walk
						.parseCommit(commit.getParent(0).getId());
				formatter = new DiffFormatter(DisabledOutputStream.INSTANCE);
				formatter.setRepository(repository);
				formatter.setDiffComparator(RawTextComparator.DEFAULT);
				formatter.setDetectRenames(true);
				List<DiffEntry> diffs = formatter.scan(parent.getTree(),
						commit.getTree());
				for (DiffEntry diff : diffs) {
					list.add(MessageFormat.format("({0} {1} {2})", diff
							.getChangeType().name(), diff.getNewMode()
							.getBits(), diff.getNewPath()));
				}
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return list;
	}

	private boolean hasCommits(Repository repository) {
		if (repository != null && repository.getDirectory().exists()) {
			return (new File(repository.getDirectory(), "objects").list().length > 2)
					|| (new File(repository.getDirectory(), "objects/pack")
							.list().length > 0);
		}
		return false;
	}

}
