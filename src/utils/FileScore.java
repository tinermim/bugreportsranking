package utils;

/**
 * Created by ramtin on 11/7/15 AD.
 */
public class FileScore implements Comparable<FileScore>{
  private String filePath;
  private double score;
  private int rank;

  public FileScore(String filePath, double s) {
    this.filePath = filePath;
    this.score = s;
  }

  public FileScore(String filePath, double s, int rank) {
    this.filePath = filePath;
    this.score = s;
    this.rank = rank;
  }

  public void setRank(int rank){this.rank = rank;}
  public int getRank(){return rank;}

  public double getScore(){return score;}
  public String getFilePath(){return filePath;}
  public void setFilePath(String path){this.filePath = path;}

  public String toString() {
    return "file: "+filePath + "\n score: " + score + "\n"+ " rank: " + rank + "\n";
  }

  @Override
  public int compareTo(FileScore o) {
    return (score - o.score) > 0 ? -1 : score == o.score ? 0 : 1;
  }

  @Override
  public boolean equals(Object obj) {
    return obj != null && obj.getClass().equals(this.getClass()) && filePath.equals(((FileScore) obj).filePath);
  }
}
