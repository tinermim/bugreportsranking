package utils;

/**
 * Created by ramtin on 15/7/13 AD.
 */
public class Debug {
    public final static boolean DEBUG = true;
    public final static void log(String text){
        if(DEBUG) System.out.println(text);
    }
}
