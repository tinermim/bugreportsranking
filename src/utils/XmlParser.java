package utils;
import java.io.File;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Created by ramtin on 11/5/15 AD.
 */
public class XmlParser {

	private File fXmlFile;
	private Document doc;

	public XmlParser(String fileName) {
		fXmlFile = new File(fileName);
		initialize();
	}

	private void initialize() {
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(fXmlFile);
			// optional, but recommended
			// read this -
			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();
			Debug.log("Root element :" + doc.getDocumentElement().getNodeName());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Created by Salar on 11/9/15 AD.
	 */
	public void saveToXML(HashMap<String, Integer> authors, String xmlSavePath) {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("rankings");
			doc.appendChild(rootElement);
			int counter = 1;
			for (Entry<String, Integer> e : authors.entrySet()) {
				Debug.log("author: " + e.getKey() + " rank: " + e.getValue());

				// staff elements
				Element staff = doc.createElement("author");
				rootElement.appendChild(staff);

				// set attribute to staff element
				Attr attr = doc.createAttribute("id");
				attr.setValue(counter + "");
				staff.setAttributeNode(attr);

				// shorten way
				// staff.setAttribute("id", "1");

				// firstname elements
				Element firstname = doc.createElement("name");
				firstname.appendChild(doc.createTextNode(e.getKey()));
				staff.appendChild(firstname);

				// lastname elements
				Element lastname = doc.createElement("score");
				lastname.appendChild(doc.createTextNode(e.getValue() + ""));
				staff.appendChild(lastname);

				counter++;

			}
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(xmlSavePath));

			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);

			transformer.transform(source, result);

			System.out.println("File saved!");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}

	public NodeList getElementsByTagName(String tag) {
		return doc.getElementsByTagName(tag);
	}

	public Element getElementById(String id) {
		return doc.getElementById(id);
	}

}
