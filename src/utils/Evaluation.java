package utils;

import java.io.File;
import java.io.IOException;
import jxl.Cell;
import jxl.CellType;
import jxl.NumberCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 * Created by ramtin on 11/9/15 AD.
 */
public class Evaluation {

  public static void main(String args[]) {
    for (int k = 1; k < 21; k++)
      Debug.log(/*"K: " + k + ", percentage: " + */String.format("%.1f", AccuracyAtK("/Users/ramtin/Desktop/NoCheckout", k, 2)) + "%");

    Debug.log("MRR: " + String.format("%.2f", MeanReciprocalRank("/Users/ramtin/Desktop/Checkout", 2)));

  }

  public static double MeanReciprocalRank(String excelFile, int columnNumber) {
    try {
      Workbook readWorkbook = Workbook.getWorkbook(new File(excelFile));
      Sheet sheet = readWorkbook.getSheet(0);
      int row = sheet.getRows();
      double sum = 0;
      int counter = 0;
      for (int i = 0; i < row; i++) {
        Cell c = sheet.getCell(columnNumber, i);
        if (c != null && c.getContents() != null && !c.getContents().equals("")) {
          if (c.getType() == CellType.NUMBER) {
            counter++;
            NumberCell nc = (NumberCell) c;
            sum += 1 / (nc.getValue());
          }
        }
      }
      // Debug.log("sum: " + sum);
      // Debug.log("counter: " + counter);

      sum = sum / counter;
      return sum;
    } catch (IOException | BiffException e) {
      e.printStackTrace();
    }
    return 0.0;
  }

  public static double AccuracyAtK(String excelFile, int k, int columnNumber) {
    try {
      Workbook readWorkbook = Workbook.getWorkbook(new File(excelFile));
      Sheet sheet = readWorkbook.getSheet(0);
      int row = sheet.getRows();
      double sum = 0;
      int counter = 0;
      for (int i = 0; i < row; i++) {
        Cell c = sheet.getCell(columnNumber, i);
        if (c != null && c.getContents() != null && !c.getContents().equals("")) {
          counter++;
          if (c.getType() == CellType.NUMBER) {
            NumberCell nc = (NumberCell) c;
            if (nc.getValue() <= k) {
              sum++;
            }
          }
        }
      }
       Debug.log("sum: " + sum);
       Debug.log("counter: " + counter);

      sum = sum / counter;
      return sum * 100;
    } catch (IOException | BiffException e) {
      e.printStackTrace();
    }
    return 0.0;
  }
}
