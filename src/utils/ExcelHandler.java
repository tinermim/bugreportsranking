package utils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import jxl.Workbook;
import jxl.write.WritableCell;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/**
 * Created by ramtin on 11/10/15 AD. This class is used to help communicating with excel files
 */
public class ExcelHandler {
  private Workbook readWorkbook = null;
  private WritableWorkbook workbook = null;
  private WritableSheet sheet = null;

  public ExcelHandler(String filePath) {
    try {
      readWorkbook = Workbook.getWorkbook(new File(Constants.EXCEL_OUTPUT_NAME));
      workbook = Workbook.createWorkbook(new File(Constants.EXCEL_OUTPUT_NAME), readWorkbook);
    } catch (Exception e) {
      try {
        workbook = Workbook.createWorkbook(new File(Constants.EXCEL_OUTPUT_NAME));
      } catch (IOException e1) {
        e1.printStackTrace();
        Debug.log("Error in creating the excel");
      }
    }
  }

  public boolean setSheet(int sheetNumber) {
    if (workbook.getSheets().length > 0) {
      sheet = workbook.getSheet(sheetNumber);
      return true;
    } else
      return false;
  }

  public boolean setSheet(String sheetName) {
    if (workbook.getSheets().length > 0) {
      sheet = workbook.getSheet(sheetName);
      return true;
    } else
      return false;
  }

  public void setOrCreateSheet(String sheetName) {
    if (!setSheet(sheetName))
      workbook.createSheet(sheetName, workbook.getNumberOfSheets());
  }

  public int getNumberOfRows() {
    return sheet.getRows();
  }

  private void writeToExcel() {
    // All sheets and cells added. Now write out the workbook
    try {
      workbook.write();

    } catch (IOException e1) {
      e1.printStackTrace();
      Debug.log("write problem");
    }
  }

  public boolean addCell(WritableCell c) {
    try {
      sheet.addCell(c);
      writeToExcel();
      return true;
    } catch (WriteException e) {
      e.printStackTrace();
      return false;
    } catch (Exception e) {
      Debug.log("sheet is null, set it before working with excel file");
      return false;
    }
  }

  public void addListOfCells(List<WritableCell> rowContent) {
    for (WritableCell c : rowContent)
      addCell(c);
    writeToExcel();
  }

  public void closeExcelFile() {
    try {
      try {
        workbook.close();
      } catch (IOException e1) {
        e1.printStackTrace();
      }
    } catch (WriteException e) {
      e.printStackTrace();
    }
  }
}
