/**
 * Created by ramtin on 15/7/8 AD.
 * Computing the textual similarity between a source code file and a bug report
 * using the standard cosine similarity between their corresponding vectors
 */
package similarity_computing;