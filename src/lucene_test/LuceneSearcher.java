package lucene_test;

/**
 *
 * Created by ramtin on 9/7/15 AD.
 */

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.DefaultSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;

import utils.Constants;

public class LuceneSearcher {
    public static final String NAME = "name";
    public static final String BODY = "body";
    public Directory fsdir;
    public IndexWriter writer;

    public String fileIndexPath;
    private static Analyzer analyzer = new StandardAnalyzer();

    public LuceneSearcher(String fileName, String commitID) throws IOException {
        fileIndexPath = Constants.INDEX_PATH + "/" + commitID + fileName;
        
//        Path indexPath = Paths.get(fileIndexPath);
//        fsdir = FSDirectory.open(indexPath);

        fsdir = new RAMDirectory();
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        writer = new IndexWriter(fsdir, config);
    }

    public void indexDoc(String docBody, String name) throws IOException {
        Document d = new Document();
        Field mainTextField = new TextField(NAME, name, Field.Store.YES);
        Field bodyTextField = new TextField(BODY, docBody, Field.Store.YES);

        d.add(mainTextField);
        d.add(bodyTextField);

        writer.addDocument(d);
    }

//    public void indexDoc(File docFile, String name) throws IOException {
//        // reading the input files
//        BufferedReader in = new BufferedReader(new FileReader(docFile));
//        String line = null;
//
//        String doc = "";
//        while ((line = in.readLine()) != null) {
//            doc += line + " ";
//        }
//
//        indexDoc(docFile, name);
//        in.close();
//    }


    public double queryUI(String query, String FieldName) throws IOException, org.apache.lucene.queryparser.classic.ParseException {
        if(writer.isOpen()) writer.close();

        // Preparing the index
//        Path indexPath = Paths.get(fileIndexPath);
//        System.out.println(fileIndexPath);
//        Directory fsdir = FSDirectory.open(indexPath);
        Analyzer analyzer = new StandardAnalyzer();
        IndexReader reader = DirectoryReader.open(fsdir);
        IndexSearcher searcher = new IndexSearcher(reader);
        // uncomment next line to switch to BM25 ranking.
        //searcher.setSimilarity(new DefaultSimilarity());

        QueryParser parser = new QueryParser(FieldName, analyzer);
        Similarity s = new DefaultSimilarity();

        // User UI
        boolean end = false;

        Query q = parser.parse(query);

        TopDocs top10 = searcher.search(q, 1);
        ScoreDoc[] results = top10.scoreDocs;

        // Iterate over and display results
        //System.out.println("Results (Top " + results.length + ")");
        reader.close();
        if(results.length > 0)
            return results[0].score;
        else
            return 0;
    }


//    public static void main(String[] args) throws IOException, ParseException {
//        File articleData = new File("/Users/ramtin/IdeaProjects/BugReportsRanking/api_stemming/org.eclipse.birt.core/src/org/eclipse/birt/core/btree/BTree/M_allocBlock.api");
//
//        // If index dir does not exist, create it and build index (need to deleted dir for re-indexing)
//        File dir = new File(Constants.INDEX_PATH);
//        if (!dir.exists()) {
//            dir.mkdirs();
//        }
////        indexDoc(articleData);
////        // Interactive UI
////        queryUI(indexPath, BODY);
//    }


}