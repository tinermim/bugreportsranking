import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import parser.JdtAstParser;
import rank_author.RankAuthor;
import utils.Constants;
import utils.Debug;
import utils.FileScore;
import utils.XmlParser;

/**
 * Created by ramtin on 11/5/15 AD.
 */
public class Main {
	/**
	 * use these constants to access the xml information
	 */
	private static int COLUMN_ID = 1;
	private static int COLUMN_BUG_ID = 3;
	private static int COLUMN_SUMMARY = 5;
	private static int COLUMN_DESCRIPTION = 7;
	private static int COLUMN_REPORT_TIME = 9;
	private static int COLUMN_REPORT_TIMESTAMP = 11;
	private static int COLUMN_STATUS = 13;
	private static int COLUMN_COMMIT = 15;
	private static int COLUMN_COMMIT_TIMESTAMP = 17;
	private static int COLUMN_FILES = 19;
	private static int COLUMN_RESULT = 21;
	private static Git git;
	private static JdtAstParser similarityCalculator;
	private static File sourceCodeDirectory;
	private static utils.XmlParser xml;

	private static String xmlAddress = "Tomcat.xml";
	private static String gitRepoAddress = "C:/Users/Salar/Desktop/tomcat/tomcat/.git";

	public static void main(String args[]) {
		xml = new XmlParser(xmlAddress);
		NodeList bugReports = xml.getElementsByTagName("table");
		sourceCodeDirectory = new File(Constants.SOURCE_CODE_PROJECT_PATH);
		similarityCalculator = new JdtAstParser();
		git = null; // checkout is the folder with .git
		
//		RankAuthor rankAuthor = new RankAuthor(xmlAddress, gitRepoAddress);

		try {
			git = Git.open(new File(gitRepoAddress));
			for (int temp = 0; temp < 1056; temp++) {
				Node nNode = bugReports.item(temp);
				long tempTime = System.currentTimeMillis();
				System.out.println("\nCurrent Element :" + temp);

				NodeList children = nNode.getChildNodes();
				Node summary = children.item(COLUMN_SUMMARY);
				Node description = children.item(COLUMN_DESCRIPTION);
				Node commit = children.item(COLUMN_COMMIT);
				Debug.log(summary.getTextContent());
				Debug.log(description.getTextContent());
				Debug.log(commit.getTextContent());
				
//				 String isCheckout = git != null &&
//				 checkout(commit.getTextContent()) ? "" : "*";

				String combination = summary.getTextContent() + "\n"
						+ description.getTextContent();
				similarityCalculator.initialize(combination,
						commit.getTextContent());
				similarityCalculator.ParseFilesInDir(sourceCodeDirectory,
						Constants.SOURCE_CODE_FILES_FORMAT, "");
				// similarityCalculator.setRankings(30);

				String[] paths = children.item(COLUMN_FILES).getTextContent()
						.split("\n");
				// String[] paths = new String[2];
				// paths[0] = "compiler/PageInfo.java";
				// paths[1] = "compiler/Validator.java";

				showFilesRank(paths, children.item(COLUMN_BUG_ID)
						.getTextContent(), "");

				Debug.log("Bug report finished in: "
						+ (System.currentTimeMillis() - tempTime) + "ms");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void showFilesRank(String[] paths, String bugId,
			String isCheckout) {
		List<FileScore> results = similarityCalculator.getFilesRanking(Arrays
				.asList(paths));
		Debug.log("...............................");
		for (FileScore result : results)
			Debug.log(result.toString());
		saveToExcel(results, bugId, isCheckout);
	}

	public static void saveToExcel(List<FileScore> results, String bugId,
			String isCheckout) {
		try {
			Workbook readWorkbook = null;
			WritableWorkbook workbook = null;
			try {
				readWorkbook = Workbook.getWorkbook(new File(
						Constants.EXCEL_OUTPUT_NAME));
				workbook = Workbook.createWorkbook(new File(
						Constants.EXCEL_OUTPUT_NAME), readWorkbook);
			} catch (Exception e) {
				workbook = Workbook.createWorkbook(new File(
						Constants.EXCEL_OUTPUT_NAME));
			}
			WritableSheet sheet;
			if (workbook.getSheets().length > 0)
				sheet = workbook.getSheet(0);
			else
				sheet = workbook.createSheet("similarity", 0);

			int row = sheet.getRows();
			jxl.write.Number label = new Number(0, row, Integer.parseInt(bugId));
			try {
				sheet.addCell(label);
			} catch (WriteException e) {
				Debug.log("write in excel problem");
				e.printStackTrace();
			}
			for (int i = 0; i < results.size(); i++) {
				Label filePath = new Label(i * 3 + 1, row, isCheckout
						+ results.get(i).getFilePath());
				jxl.write.Number fileScore = new Number(i * 3 + 2, row, results
						.get(i).getScore());

				jxl.write.Number fileRank= new Number(i * 3 + 3, row, results
						.get(i).getRank());

				try {
					sheet.addCell(filePath);
					sheet.addCell(fileScore);
					sheet.addCell(fileRank);
				} catch (WriteException e) {
					Debug.log("write in excel problem");
					e.printStackTrace();
				}
			}
			// All sheets and cells added. Now write out the workbook
			workbook.write();
			try {
				workbook.close();
			} catch (WriteException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static boolean checkout(String commitId) {
		try {
			long tempTime = System.currentTimeMillis();
			git.checkout().setName(commitId).call(); // succeeds
			Debug.log("Checkout done...");
			Debug.log("Checkout finished in: "
					+ (System.currentTimeMillis() - tempTime) + "ms");
			return true;
		} catch (GitAPIException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			return false;
		}
	}
}
