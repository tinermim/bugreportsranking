//package parser;
//
//
//import java.nio.charset.Charset;
//import java.util.ArrayList;
//import java.util.Map;
//import org.eclipse.jdt.core.JavaCore;
//import org.eclipse.jdt.core.dom.AST;
//import org.eclipse.jdt.core.dom.ASTParser;
//import org.eclipse.jdt.core.dom.CompilationUnit;
//import utils.Constants;
//
//public class ASTBinder {
//
//    public static String[] getClassPath(){
//        String classpath = JdtAstParser.readFileToString(Constants.CLASS_PATH_FILE, Charset.defaultCharset());
//        return classpath.split("\\n");
//    }
//
//    public static void main(String[] args) {
//        String str = JdtAstParser.readFileToString(Constants.SOURCE_CODE_PROJECT_PATH + "/tokenizer/ASTBinder.java",
//                Charset.defaultCharset());
//
//        ArrayList<Constants> consts = new ArrayList<>();
//        consts.add(new Constants());
//
//        ASTParser parser = ASTParser.newParser(AST.JLS3);
//        parser.setResolveBindings(true);
//        parser.setKind(ASTParser.K_COMPILATION_UNIT);
//
//        parser.setBindingsRecovery(true);
//
//        Map options = JavaCore.getOptions();
//        parser.setCompilerOptions(options);
//        parser.setSource(str.toCharArray());
//
//        String unitName = "ASTBinder.java";
//        parser.setUnitName(unitName);
//
//        String[] sources = { Constants.SOURCE_CODE_PROJECT_PATH};
//        String[] classpath = getClassPath();
//
//        parser.setEnvironment(classpath, sources, new String[] { "UTF-8"}, true);
//
//
//        CompilationUnit cu = (CompilationUnit) parser.createAST(null);
//
//        if (cu.getAST().hasBindingsRecovery()) {
//            System.out.println("Binding activated.");
//        }
//
//        TypeFinderVisitor v = new TypeFinderVisitor();
//        cu.accept(v);
//    }
//}