package parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import lucene_test.LuceneSearcher;

import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.queryparser.classic.ParseException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;

import rank_author.RankAuthor;
import tokenizer.SourceCodeTokenizer;
import utils.Constants;
import utils.Debug;
import utils.FileScore;

/**
 * Created by ramtin on 15/7/10 AD. This class is an API for eclipse jdt AST
 * API. The AST records the source code structure, javadoc and comments. It is
 * also possible to change the AST nodes or create new ones to modify the source
 * code.
 */
public class JdtAstParser {

	public static long sum_time = 0;
	public static long counter = 0;
	public String bugReport = null;
	public TreeSet<utils.FileScore> scores;
	public String commitID;
	public RankAuthor authors;

	public JdtAstParser() {
		this.authors = new RankAuthor("Tomcat.xml",
				Constants.SOURCE_CODE_PROJECT_PATH + "/.git");
	}

	// read file content into a string
	public static String readFileToString(String filePath, Charset encoding) {
		byte[] encoded = new byte[0];
		try {
			encoded = Files.readAllBytes(Paths.get(filePath));
		} catch (IOException e) {
			System.out
					.println("file not found at location: "
							+ filePath
							+ "\n, maybe you should change the project and class path located in utils.Constants.java file");
			e.printStackTrace();
		}
		return new String(encoded, encoding);
	}

	public void initialize(String bugReport, String commitID) {
		this.bugReport = SourceCodeTokenizer.AnalyzeUsingLucene(bugReport,
				StandardAnalyzer.class);

		this.commitID = commitID;
		this.scores = new TreeSet<>();
	}

	private CompilationUnit getCompilationUnit(String str) {
		ASTParser parser = ASTParser.newParser(AST.JLS3);
		parser.setSource(str.toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);

		return (CompilationUnit) parser.createAST(null);
	}

	public MethodDeclaration[] getMethods(String fileContent) {
		final CompilationUnit cu = getCompilationUnit(fileContent);
		List<TypeDeclaration> types = cu.types();
		ArrayList<MethodDeclaration> methods = new ArrayList<>();

		for (TypeDeclaration type : types) {
			MethodDeclaration[] members = type.getMethods();
			for (MethodDeclaration method : members) {
				methods.add(method);
			}
		}

		return methods.toArray(new MethodDeclaration[methods.size()]);
	}

	private VariableDeclarationStatement[] getMethodVariables(
			MethodDeclaration method) {
		ArrayList<VariableDeclarationStatement> variables = new ArrayList<>();
		method.accept(new ASTVisitor() {
			public boolean visit(VariableDeclarationStatement fd) {
				variables.add(fd);
				return true;
			}
		});

		return variables.toArray(new VariableDeclarationStatement[variables
				.size()]);
	}

	/**
	 * use ASTParse to parse string
	 */
	public void parse(String fileName, String str, String filePath)
			throws IOException, ParseException {
		// System.out.println("filname: " + fileName);
		// System.out.println("str: " + str);
		// System.out.println("filePath: " + filePath);
		counter++;
		long temp = System.currentTimeMillis();
		LuceneSearcher lucene = new LuceneSearcher(filePath + "/"
				+ fileName.split("\\.")[0], commitID);
		String fileDirectory = Constants.STEMMED_FILES_PATH + "/" + filePath
				+ "/" + fileName.split("\\.")[0];
		// File theDir = new File(fileDirectory);
		// System.out.println("directory: " + theDir.getAbsolutePath());
		// if (!theDir.exists())
		// theDir.mkdirs();

		// if (bugReport.contains(fileName)) {
		// System.out.println("Class Name Similarity: " + fileName.length());
		// } else {
		// System.out.println("Class Name Similarity: " + 0);
		// }

		lucene.indexDoc(str, fileName.split("\\.")[0]);
		MethodDeclaration[] methods = getMethods(str);
		for (MethodDeclaration method : methods)
			lucene.indexDoc(method.getBody() + "", method.getName() + "");

		// System.out.println("parsing file: " + fileName);

		double score = processScore(
				lucene.queryUI(bugReport, LuceneSearcher.BODY), filePath + "/"
						+ fileName);

		if (Double.compare(score, 0.0) != 0)
			scores.add(new FileScore(fileDirectory, score));

		sum_time += System.currentTimeMillis() - temp;
	}

	public void setRankings(int numberOfResults) {
		Debug.log("...............................");
		Debug.log("size: " + scores.size());
		if (numberOfResults <= 0)
			numberOfResults = scores.size();
		Iterator<FileScore> iterator = scores.iterator();
		int counter = 0;

		while (iterator.hasNext() && counter++ < numberOfResults) {
			FileScore fs = iterator.next();
			Debug.log("[" + counter + "]" + fs + "");
		}
	}

	public List<FileScore> getFilesRanking(List<String> paths) {
		Debug.log("...............................");
		long temp_time = System.currentTimeMillis();
		Iterator<FileScore> iterator = scores.iterator();
		ArrayList<FileScore> buggyFilesRank = new ArrayList<>();
		int counter = 1;

		while (iterator.hasNext()) {
			FileScore fs = iterator.next();
			fs.setRank(counter++);
			for (String p : paths) {
				if ((fs.getFilePath() + ".java").contains(p)) {
					FileScore temp = new FileScore(p, fs.getScore(),
							fs.getRank());
					buggyFilesRank.add(temp);
					break;
				}
			}
		}

		Debug.log("average parse time(time: " + sum_time + ", counter: "
				+ counter + "): " + (sum_time / counter)
				+ "ms, getFilesRankingTime: "
				+ (System.currentTimeMillis() - temp_time) + "ms");
		return buggyFilesRank;
	}

	private double processScore(double rawSimilarityScore, String path) {
		String[] mainPath = path.split("/");
		path = mainPath[mainPath.length - 4] + "/"
				+ mainPath[mainPath.length - 3] + "/"
				+ mainPath[mainPath.length - 2] + "/"
				+ mainPath[mainPath.length - 1];
		if (authors.getFileScore(path) != 0)
			return 0.4
					* scaleScore(0, 0.026979178334768046,
							authors.getFileScore(path)) + 0.6
					* scaleScore(0, 2.02789950370788 , rawSimilarityScore);
		else
			return scaleScore(0, 1.5, rawSimilarityScore);
	}

	private double scaleScore(double min, double max, double givenNumber) {
		double out = (givenNumber - min) / (max - min);
		if (out > 1)
			return 1;
		if (out < 0)
			return 0;
		return out;
	}

	private void makeApiDirectoryForSourceCode(String str,
			String fileDirectory, String fileName) {
		String tokens = SourceCodeTokenizer.AnalyzeUsingLucene(str,
				EnglishAnalyzer.class);
		try {
			String stem = "";
			System.out.println("file: " + fileDirectory + "/" + "S_"
					+ fileName.split("\\.")[0] + ".api");
			PrintWriter out = new PrintWriter(fileDirectory + "/" + "S_"
					+ fileName.split("\\.")[0] + ".api");
			for (String token : tokens.split("//s+"))
				stem += token + " ";
			out.print(stem);
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void makeApiDirectoryForMethod(MethodDeclaration method,
			String fileDirectory) {
		String tokens;
		System.out.println("\n      in method: " + method.getName());
		tokens = SourceCodeTokenizer.AnalyzeUsingLucene(method.getJavadoc()
				+ " " + method.getBody(), EnglishAnalyzer.class);
		try {
			String stem = "";
			System.out.println("file: " + fileDirectory + "/M_"
					+ method.getName() + ".api");
			PrintWriter out = new PrintWriter(fileDirectory + "/" + "M_"
					+ method.getName() + ".api");
			for (String token : tokens.split("//s+"))
				stem += token + " ";
			out.print(stem);
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void printVariables(MethodDeclaration method) {
		VariableDeclarationStatement[] variables = getMethodVariables(method);
		for (VariableDeclarationStatement variable : variables)
			System.out.println("           variable type: "
					+ variable.getType());
	}

	/**
	 * loop directory to get file list. regex Extension shows files to be parsed
	 * based on the extension. each extension should be separated with "|"
	 * character e.g. java|class|txt
	 */

	public void ParseFilesInDir(File folder, String regexExtensions,
			String relativePath) throws IOException {
		// regexExtensions = "[^/s]+(.(?i)(" + regexExtensions + "))$";

		File[] listOfFiles = folder.listFiles();

		for (File file : listOfFiles) {
			if (file.isFile()) {
				String extension = "";
				int i = file.getName().lastIndexOf('.');
				if (i > 0) {
					extension = file.getName().substring(i + 1);
				}
				// System.out.println(file.getName() + " ,extension: "
				// +extension+"/"+regexExtensions +" :"+
				// extension.equals(regexExtensions));
				if (!(extension.endsWith(regexExtensions)))
					continue;
				// System.out.println("  File name: " + file.getName());
				try {
					parse(file.getName(),
							readFileToString(file.getPath(),
									Charset.defaultCharset()), relativePath);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				// System.out.println("\n");
			} else if (file.isDirectory()) {
				// System.out.println("Directory name: " + file.getName());
				ParseFilesInDir(file, regexExtensions, relativePath + "/"
						+ file.getName());
			}
		}
	}

	// public static void main(String args[]) throws IOException {
	// bugReport =
	// "Bug 422205  Missing help content reference in MongoDB ODA Data Set UI page. The help context ID is not referenced by the MongoDB DOA data set query page.";
	//
	// System.out.println("process start...");
	// File SourceCodeDirectory = new File(Constants.SOURCE_CODE_PROJECT_PATH);
	// ParseFilesInDir(SourceCodeDirectory, Constants.SOURCE_CODE_FILES_FORMAT,
	// "");
	// System.out.println("project stemming end. result available on " +
	// Constants.STEMMED_FILES_PATH);
	// }
}
