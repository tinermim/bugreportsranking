/**
 * Created by ramtin on 15/7/10 AD.
 *
 * When the source file is large, its corresponding norm will also be large, which will result in a small
 * cosine similarity with the bug report, even though one method in the file may be actually very relevant
 * for the same bug re- port. Therefore, we use the AST parser and segment the source code into methods in
 * order to compute per-method similarities with the bug report. We consider each method m as a separate document.
 * Similarity is calculated using the whole source code and its method separately, using Lucene DefaultSimilarity class
 * and
 * @see lucene_test.LuceneSearcher
 */
package parser;