package parser;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseException;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.BodyDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * Created by ramtin on 15/7/10 AD.
 * Java 1.8 Parser and Abstract Syntax Tree for Java.
 * This class is an API for javaParser library which contains a Java 1.8 Parser with AST generation and visitor support.
 * The AST records the source code structure, javadoc and comments. It is also possible to change the AST nodes
 * or create new ones to modify the source code.
 */
public final class AbstractSyntaxTree {
    private CompilationUnit cu = null;

    public AbstractSyntaxTree(String fileName){
        // creates an input stream for the file to be parsed
        FileInputStream in = null;
        try {
            in = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            // parse the file
            try {
                cu = JavaParser.parse(in, "UTF-8", true /*consider comments*/);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String CompilationUnitPrinter() {
        // prints the resulting compilation unit to default system output
        return cu.toString();
    }


    public void MethodPrinter(){
        List<TypeDeclaration> types = cu.getTypes();
        for (TypeDeclaration type : types) {

            List<BodyDeclaration> members = type.getMembers();
            for (BodyDeclaration member : members) {
                if (member instanceof MethodDeclaration) {
                    MethodDeclaration method = (MethodDeclaration) member;
                    System.out.println("\n new Method: ");
                    System.out.println(method.toString());

                }
            }
        }

    }

    /**
     * Simple visitor implementation for visiting MethodDeclaration nodes.
     */
    private static class MethodVisitor extends VoidVisitorAdapter {

        @Override
        public void visit(MethodDeclaration n, Object arg) {
            // here you can access the attributes of the method.
            // this method will be called for all methods in this
            // CompilationUnit, including inner class methods

            System.out.println(n.getName());
        }
    }

    /**
     * Test
     * @param args
     */
    public static void main(String[] args){
        AbstractSyntaxTree parser = new AbstractSyntaxTree("src/tokenizer/SourceCodeTokenizer.java");
        parser.MethodPrinter();
    }
}





