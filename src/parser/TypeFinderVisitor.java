package parser;

import org.eclipse.jdt.core.dom.*;
import utils.Debug;

import java.util.Iterator;

/**
 * Created by ramtin on 11/6/15 AD.
 */
class TypeFinderVisitor extends ASTVisitor {

    public boolean visit(MethodDeclaration node) {
        System.out.println("method found: " + node.getName());

        node.accept(new ASTVisitor() {
            public boolean visit(MethodInvocation node) {
                Expression exp = node.getExpression();
                Debug.log("    Method: ");
                ITypeBinding typeBinding = node.getExpression().resolveTypeBinding();
                //System.out.println("    method Type: " + typeBinding.getName());
                Debug.log("        name: " + node.getName());
                try {
                    Debug.log("        ClassPath:  " + typeBinding.getQualifiedName());
                } catch (NullPointerException e) {
                    Debug.log("        Class path not found, class ignored...");
                }
                return true;
            }

            public boolean visit(VariableDeclarationStatement node) {
                Debug.log("    Variable: ");
                for (Iterator iter = node.fragments().iterator(); iter.hasNext(); ) {

                    VariableDeclarationFragment fragment = (VariableDeclarationFragment) iter.next();
                    IVariableBinding binding = fragment.resolveBinding();

                    Debug.log("        name: " + binding.getName());
                    //System.out.println("    type: " +node.getType());
                    Debug.log("        ClassPath: " + binding.getVariableDeclaration().toString().split("\\s+")[0]);
                    //System.out.println("    method declaration: " +binding.getDeclaringMethod());
                }

                return true;
            }

        });

        Debug.log("---------------------");

        return true;
    }
}