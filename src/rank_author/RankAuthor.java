package rank_author;

import git.CrawlHandler;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import utils.Debug;
import utils.XmlParser;

public class RankAuthor {

	private static int COLUMN_FILES = 19;
	private static int COLUMN_REPORT_TIMESTAMP = 11;
	private CrawlHandler crawlHandler;
	private ArrayList<String> buggyFiles;
	private ArrayList<String> buggyFileReportDate;
	private HashMap<String, Integer> authorPoints;
	private HashMap<String, String> fileAuthorMap;
	private HashMap<String, Integer> fileAuthorWeight;
	private String bugReportAddress;
	private String gitRepoAddress;
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");;
	private Date date;
	private XmlParser xml;
	private long toalFileScore;

	public RankAuthor(String bugReportAddress, String gitRepoAddress) {
		this.bugReportAddress = bugReportAddress;
		this.gitRepoAddress = gitRepoAddress;
		buggyFiles = parseXml();
		Debug.log("Number of buggy files in the bugreport file: "
				+ buggyFiles.size());
		authorPoints = new HashMap<String, Integer>();
		fileAuthorMap = new HashMap<String, String>();
		fileAuthorWeight = new HashMap<String, Integer>();
		try {
			date = new Date();
			Debug.log("git initialization started, at time: "
					+ dateFormat.format(date));
			crawlHandler = new CrawlHandler(this.gitRepoAddress);
			date = new Date();
			Debug.log("git initialization ended, at time: "
					+ dateFormat.format(date));
		} catch (IOException e) {
			e.printStackTrace();
		}
		findBuggyAuthors();
		this.toalFileScore = getTotalFileScore();
		// xml.saveToXML(authorPoints, xmlSavePath);
	}

	private void findBuggyAuthors() {
		date = new Date();
		Debug.log("finding buggy authors started, at time: "
				+ dateFormat.format(date));
		for (int i = 0; i < buggyFiles.size(); i++) {
			Debug.log("checking the bug number " + (i + 1));
			Set<String> modifierAuthors = crawlHandler.getModifierAuthors(
					buggyFiles.get(i), buggyFileReportDate.get(i) + "000");
			updateAuthorRankings(modifierAuthors);
			updateFileRankings(buggyFiles.get(i), modifierAuthors);
		}
		date = new Date();
		Debug.log("finding buggy aauthors ended, at time: "
				+ dateFormat.format(date));
	}

	private void updateFileRankings(String fileName, Set<String> modifierAuthors) {
		ArrayList<String> list = new ArrayList<String>(modifierAuthors);
		if (fileAuthorMap.get(fileName) == null
				|| fileAuthorMap.get(fileName).length() == 0) {
			String allAuthors = "";
			for (String author : list) {
				allAuthors = allAuthors + author + "-";
				if (fileAuthorWeight.get(fileName + "-" + author) == null)
					fileAuthorWeight.put(fileName + "-" + author, 1);
				else
					fileAuthorWeight.put(fileName + "-" + author,
							fileAuthorWeight.get(fileName + "-" + author) + 1);
			}
			fileAuthorMap.put(fileName, allAuthors);
			System.out.println("^^^^^^ " + fileName);
		} else {
			String allAuthors = fileAuthorMap.get(fileName);
			for (String author : list) {
				if (!allAuthors.contains(author)) {
					allAuthors = allAuthors + author + "-";
					fileAuthorWeight.put(fileName + "-" + author, 1);
				} else {
					fileAuthorWeight.put(fileName + "-" + author,
							fileAuthorWeight.get(fileName + "-" + author) + 1);
				}
			}
		}
	}

	private void updateAuthorRankings(Set<String> modifierAuthors) {
		date = new Date();
		Debug.log("ranking authors started, at time: "
				+ dateFormat.format(date));
		ArrayList<String> list = new ArrayList<String>(modifierAuthors);
		for (int i = 0; i < list.size(); i++) {
			if (authorPoints.get(list.get(i)) != null) {
				authorPoints
						.put(list.get(i), authorPoints.get(list.get(i)) + 1);
			} else {
				authorPoints.put(list.get(i), 1);
			}
		}

		for (Entry<String, Integer> e : authorPoints.entrySet()) {
			Debug.log("author: " + e.getKey() + " rank: " + e.getValue());
		}
		date = new Date();
		Debug.log("ranking authors ended, at time: " + dateFormat.format(date));
	}

	private ArrayList<String> parseXml() {
		buggyFileReportDate = new ArrayList<String>();

		date = new Date();
		Debug.log("parsing xml started, at time: " + dateFormat.format(date));
		xml = new XmlParser(bugReportAddress);
		NodeList bugReports = xml.getElementsByTagName("table");
		ArrayList<String> list = new ArrayList<String>();

		for (int temp = 0; temp < bugReports.getLength(); temp++) {
			Node nNode = bugReports.item(temp);
			NodeList children = nNode.getChildNodes();
			Node files = children.item(COLUMN_FILES);
			String[] allFiles = files.getTextContent().split("\n");
			list.addAll(Arrays.asList(allFiles));

			Node timeStamp = children.item(COLUMN_REPORT_TIMESTAMP);
			int diff = list.size() - buggyFileReportDate.size();
			for (int i = 0; i < diff; i++)
				buggyFileReportDate.add(timeStamp.getTextContent());
		}

		date = new Date();
		Debug.log("parsing xml ended, at time: " + dateFormat.format(date));

		return list;
	}

	public double getFileScore(String fileName) {
		if (fileAuthorMap.get(fileName) == null)
			return 0;
		String[] allAuthors = fileAuthorMap.get(fileName).split("-");
		long fileScore = 0;
		for (String string : allAuthors) {
			if (authorPoints.get(string) == null)
				continue;
			fileScore += authorPoints.get(string)
					* fileAuthorWeight.get(fileName + "-" + string);
		}
		return ((double) fileScore / (double) toalFileScore);
	}

	private long getTotalFileScore() {
		long out = 0;
		for (Map.Entry<String, String> entry : fileAuthorMap.entrySet()) {
			String[] allAuthors = entry.getValue().split("-");
			for (String string : allAuthors) {
				if (authorPoints.get(string) == null)
					continue;
				out += authorPoints.get(string)
						* fileAuthorWeight.get(entry.getKey() + "-" + string);
			}
		}
		System.out.println("**** the total point: " + out);
		return out;
	}

}
